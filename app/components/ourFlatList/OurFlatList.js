import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const DATA = [
  {
    id: '1',
    title: 'Iron Man',
    description: 'Tony Stark',
    picURL: 'https://static.posters.cz/image/750/poster/marvel-iron-man-cover-i13712.jpg'
  },
  {
    id: '2',
    title: 'Capitan America',
    description: 'Steve Rogers',
    picURL: 'https://cloud10.todocoleccion.online/comics-vertice/tc/2016/02/18/00/54526177.jpg'
  },
  {
    id: '3',
    title: 'Thor',
    description: 'Hijo de Odin',
    picURL: 'https://i.pinimg.com/originals/04/f8/9c/04f89c3ca0846d602309548ee74fb549.jpg'
  },
  {
    id:'4',
    title:'La Viuda Negra',
    description:'Natasha Romanova',
    picURL:'https://images-na.ssl-images-amazon.com/images/I/518rnaeEedL._SX328_BO1,204,203,200_.jpg'
  },
  {
    id:'5',
    title:'Vision',
    description:'Vision',
    picURL:'https://img.vixdata.io/pd/jpg-large/es/sites/default/files/btg/comics.batanga.com/files/La-portada-del-dia-Avengers-57-2.jpg'
  },
  {
    id:'6',
    title:'Hulk',
    description:'Robert Bruce Banner',
    picURL:'https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/691507/691507._SX360_QL80_TTD_.jpg'
  },

];

function Item({title,image}) {
  return (
    <View style={styles.item}>
      <Image style={styles.image} source={{uri: image}}/>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default class OurFlatList extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>
        {/*<FlatList
          data={DATA}
          renderItem={({item}) => <Item title={item.description} />}
          keyExtractor={item => item.id}
        />*/}
        <ScrollView>
          {DATA.map(item => (
            <TouchableOpacity style={styles.item} onPress={()=> this.props.navigation.navigate('Details',{
              key: item.id,
              itemID: item.id,
              itemTitle: item.title,
              itemDesc: item.description,
              itemPic: item.picURL})}>
              
              <Item title={item.title} image={item.picURL}/>
              
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );

  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //marginTop: 20,
    backgroundColor: '#212121'
  },
  item: {
    flexDirection: 'row',
    backgroundColor: '#0277bd',
    padding: 1,
    marginVertical: 5,
    marginHorizontal: 5
  },
  title: {
    fontSize: 32,
    textAlignVertical: "center",
    marginStart: 15,
    color: 'white'
  },
  image:{
    height: 80,
    width: 80,
    borderColor: '#212121',
    borderWidth: 5,
    padding: 15
  }
});
